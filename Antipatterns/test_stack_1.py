from Specifications.stack import Stack


def test_stack():
    lines = list(map(lambda x: (x[0] == 'push', x[1]),
                     map(lambda s: s.split(),
                         open(r"D:\test.txt", 'r').readlines())))

    stack = Stack()
    for line in lines:
        if line[0]:
            stack.push(line[1])
        else:
            assert line[1] == stack.pop()


#region Почему это плохо?
#          ## Антипаттерн Local Hero
#
#    		Тест не будет работать на машине другого человека или на Build-сервере.
#    		Да и у того же самого человека после форматирования жесткого диска / переустановки ОС / повторного Clone репозитория / ...
#
#   		## Решение
#
#    		Тест не должен зависеть от особенностей локальной среды.
#    		Если нужна работа с файлами, то либо включите файл в проект.
#
#    		lines = open("data.txt")
#endregion