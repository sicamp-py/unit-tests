from Specifications.stack import Stack


def test_stack():
    stack = Stack()
    stack.push(10)
    stack.push(20)
    stack.push(30)
    while not stack.empty():
        print(stack.pop())

#region Почему это плохо?
#            ## Антипаттерн Loudmouth
#
#            Тест не является автоматическим. Если он сломается, этого никто не заметит.
#
#            ## Мораль
#            Вместо вывода на консоль, используйте assert'ы
#endregion