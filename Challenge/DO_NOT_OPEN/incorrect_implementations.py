from Challenge.word_statistics import WordStatistics
from collections import defaultdict


class WordStatistics_I01(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        self._stats[word.lower()] += 1


class WordStatistics_I02(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        elif len(word) > 5:
            word = word[:-1]
        self._stats[word.lower()] += 1


class WordStatistics_I03(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) - 1 > 10:
            word = word[:10]
        self._stats[word.lower()] += 1


class WordStatistics_I04(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[word] += 1


class WordStatistics_I05(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word) > 10:
            word = word[:10]
        self._stats[word.lower()] += 1


class WordStatistics_I06(WordStatistics):
    def add_word(self, word):
        if word is None:
            return
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[word.lower()] += 1


class WordStatistics_I07(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            return
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[word.lower()] += 1


class WordStatistics_I08(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip(' ')) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[word.lower()] += 1


class WordStatistics_I09(WordStatistics):
    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word) > 10:
            word = word[:10]
        if len(word.strip()) == 0:
            return
        self._stats[word.lower()] += 1


class WordStatistics_I10(WordStatistics):
    def get_statistics(self):
        ordered_by_key = sorted(self._stats.items(), key=lambda pair: pair[0])
        return list(map(lambda pair: (pair[1], pair[0]), ordered_by_key))


class WordStatistics_I11(WordStatistics):
    def get_statistics(self):
        ordered_by_key_value_desc = sorted(self._stats.items(), key=lambda pair: pair[1], reverse=True)
        return list(map(lambda pair: (pair[1], pair[0]), ordered_by_key_value_desc))


class WordStatistics_I12(WordStatistics):
    def get_statistics(self):
        return list(sorted(super().get_statistics(), key=lambda pair: pair[1]))


class WordStatistics_I13(WordStatistics):
    def get_statistics(self):
        return list(sorted(super().get_statistics(), reverse=True))


class WordStatistics_I14:
    _stats = defaultdict(int)

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        WordStatistics_I14._stats[word.lower()] += 1

    def get_statistics(self):
        ordered_by_key = sorted(WordStatistics_I14._stats.items(), key=lambda pair: pair[0])
        WordStatistics_I14._stats.clear()
        ordered_by_key_value_desc = sorted(ordered_by_key, key=lambda pair: pair[1], reverse=True)
        return list(map(lambda pair: (pair[1], pair[0]), ordered_by_key_value_desc))


class WordStatistics_I15:

    def __init__(self):
        self.modulo = 12347
        self.counts = [0] * self.modulo
        self.words = [''] * self.modulo

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        word = word.lower()
        index = abs(hash(word)) % self.modulo
        self.counts[index] += 1
        self.words[index] = word.lower()

    def get_statistics(self):
        zipped = filter(lambda pair: pair[0] > 0, zip(self.counts, self.words))
        return sorted(sorted(zipped, key=lambda pair: pair[1]), key=lambda pair: pair[0], reverse=True)


class WordStatistics_I16(WordStatistics):

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[self._lower(word)] += 1

    def _lower(self, word):
        return ''.join(map(self._to_lower, word))

    @staticmethod
    def _to_lower(char):
        if char in "QWERTYUIOPASDFGHJKLZXCVBNM":
            return chr(ord(char) - (ord('D') - ord('d')))
        if char in "ЙЦУКЕНГШЩЗФЫВАПРОЛДЯЧСМИТЬ":
            return chr(ord(char) - (ord('Я') - ord('я')))
        return char


class WordStatistics_I17:

    def __init__(self):
        self._stats = list()

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        word = word.lower()
        index = self._find_index_of_word(word)
        if index < 0:
            self._stats.append((-1, word))
        else:
            self._stats.append((self._stats[index][0] - 1, self._stats[index][1]))
            del (self._stats[index])
        self._stats.sort()

    def _find_index_of_word(self, word):
        index = 0
        for k, v in self._stats:
            if word == v:
                return index
            index += 1
        return -1

    def get_statistics(self):
        return list(map(lambda pair: (-pair[0], pair[1]), self._stats))


class WordStatistics_I18(WordStatistics):
    def get_statistics(self):
        ordered_by_key = sorted(self._stats.items(), key=lambda pair: pair[0])
        self._stats.clear()
        ordered_by_key_value_desc = sorted(ordered_by_key, key=lambda pair: pair[1], reverse=True)
        return list(map(lambda pair: (pair[1], pair[0]), ordered_by_key_value_desc))


class WordStatistics_I19(WordStatistics):
    def __init__(self):
        super().__init__()
        self._result = None

    def get_statistics(self):
        if self._result is None:
            self._result = super().get_statistics()
        return self._result


class WordStatistics_I20:

    def __init__(self):
        self._used_words = set()
        self._stats = list()

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        word = word.lower()
        if word in self._used_words:
            index = self._find_index_of_word(word)
            c, w = self._stats[index]
            new_tuple = (c + 1, w)
            del (self._stats[index])
            self._stats.append(new_tuple)
        else:
            self._stats.append((1, word))
            self._used_words.add(word)

    def _find_index_of_word(self, word):
        index = 0
        for k, v in self._stats:
            if v == word:
                return index
            index += 1
        return -1

    def get_statistics(self):
        ordered_by_value = sorted(self._stats, key=lambda pair: pair[1])
        return sorted(ordered_by_value, key=lambda pair: pair[0], reverse=True)
