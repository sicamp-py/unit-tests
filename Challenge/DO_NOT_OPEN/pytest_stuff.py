import pytest
from contextlib import redirect_stdout
import os


class SubstituteImplementationPlugin:
    def __init__(self, impl):
        self.__name__ = 'substitute'
        self.impl = impl

    @pytest.fixture(scope='module', autouse=True)
    def patch_module(self, request):
        plugin = request.config.pluginmanager.get_plugin('substitute')
        module_to_patch = request.module
        module_to_patch.WordStatistics = plugin.impl

class CollectTestsResultsPlugin:
    def __init__(self):
        self.failed_tests = []

    @pytest.hookimpl(tryfirst=True, hookwrapper=True)
    def pytest_runtest_makereport(self, item, call):
        # execute all other hooks to obtain the report object
        outcome = yield
        result = outcome.get_result()

        # we only look at actual failing test calls, not setup/teardown
        if result.when == "call" and result.failed:
            self.failed_tests.append(result)

def test_implementation(implementation, pytest_args):
    substitute = SubstituteImplementationPlugin(implementation)
    results = CollectTestsResultsPlugin()

    with redirect_stdout(open(os.devnull, 'w')):
        exit_code = pytest.main(pytest_args, plugins=[substitute, results])

    failed_tests = [ test.nodeid.split('::')[-1] for test in results.failed_tests]
    was_successful = (exit_code == 0)
    return (was_successful, failed_tests)
