import unittest
import io

runner = unittest.TextTestRunner(stream=io.StringIO(), verbosity=0)
test_loader = unittest.TestLoader()

def test_implementation(implementation, tests):
    test_suite = make_test_suite(implementation, tests)
    result = runner.run(test_suite)
    failed_tests = get_test_names(result.failures)
    return (result.wasSuccessful(), failed_tests)

def make_test_suite(implementation, tests):
    tests.createStatistics = lambda self: implementation()
    return test_loader.loadTestsFromTestCase(tests)

def get_test_names(failures):
    return [test._testMethodName for test, _ in failures]
