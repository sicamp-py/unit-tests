import unittest
from Challenge.word_statistics import WordStatistics
from timeit import default_timer

# Сначала впишите свою имя и фамилию
THE_AUTHOR = 'Антон Чаплыгин'


class WordStatistics_Tests(unittest.TestCase):
    """Пишите тесты здесь"""

    def setUp(self):
        self.statistics = self.createStatistics()

    def createStatistics(self):
        return WordStatistics()

    def test_Against_NotCutting10Symbols(self):
        self.statistics.add_word('a' * 12)
        self.statistics.add_word('a' * 13)
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_Border5(self):
        self.statistics.add_word("aaaaa")
        self.statistics.add_word("aaaaaa")
        self.assertEqual(2, len(self.statistics.get_statistics()))

    def test_Against_Border11(self):
        self.statistics.add_word('a' * 10)
        self.statistics.add_word('a' * 11)
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_CaseSensitivity(self):
        self.statistics.add_word("a")
        self.statistics.add_word("A")
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_CountingWhiteSpaceWords(self):
        self.statistics.add_word(" ")
        self.statistics.add_word("a")
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_AddingNone(self):
        self.assertRaises(TypeError, lambda: self.statistics.add_word(None))

    def test_Against_AddingNotString(self):
        self.assertRaises(TypeError, lambda: self.statistics.add_word(5))

    def test_Against_WordWithNon32Whitespace(self):
        self.statistics.add_word("\n")
        self.assertEqual(0, len(self.statistics.get_statistics()))

    def test_Against_CutTo10ThenCheckForWhitespace(self):
        self.statistics.add_word(" " * 10 + "a")
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_OrderByCount(self):
        self.statistics.add_word('a')
        self.statistics.add_word('b')
        self.statistics.add_word('b')
        self.assertListEqual([(2, 'b'), (1, 'a')], self.statistics.get_statistics())

    def test_Against_NotOrderingByWord(self):
        self.statistics.add_word('a')
        self.statistics.add_word('b')
        self.statistics.add_word('c')
        self.statistics.add_word('d')
        self.statistics.add_word('e')
        self.statistics.add_word('f')
        self.assertListEqual([(1, 'a'), (1, 'b'), (1, 'c'), (1, 'd'), (1, 'e'), (1, 'f')],
                             self.statistics.get_statistics())

    def test_Against_OrderByWordDesc(self):
        self.statistics.add_word('a')
        self.statistics.add_word('b')
        self.assertListEqual([(1, 'a'), (1, 'b')], self.statistics.get_statistics())

    def test_Against_StaticFieldOrClearAfterGet(self):
        self.statistics.add_word('a')
        new_stats = self.createStatistics()
        self.assertEqual(0, len(new_stats.get_statistics()))

    def test_Against_AllLetters(self):
        a, z = ord('A'), ord('Z')
        rus_a, rus_ya = ord('А'), ord('Я')
        for c in range(a, z + 1):
            self.statistics.add_word(chr(c))
            self.statistics.add_word(chr(c).lower())
        for c in range(rus_a, rus_ya + 1):
            self.statistics.add_word(chr(c))
            self.statistics.add_word(chr(c).lower())
        self.assertEqual(z - a + 1 + rus_ya - rus_a + 1, len(self.statistics.get_statistics()))

    def test_Against_Hash_And_LongAdd(self):
        generator = word_generator(5, list())
        count = 12350
        start_time = default_timer()
        for i in range(count):
            word = next(generator)
            self.statistics.add_word(word)
            self.assertLess(default_timer() - start_time, 1.0)
        self.assertEqual(count, len(self.statistics.get_statistics()))

    def test_Against_GetStatisticsTwice(self):
        self.statistics.add_word('a')
        self.assertEqual(1, len(self.statistics.get_statistics()))
        self.assertEqual(1, len(self.statistics.get_statistics()))

    def test_Against_GetStatisticsCache(self):
        self.statistics.add_word('a')
        self.assertEqual(1, len(self.statistics.get_statistics()))
        self.statistics.add_word('b')
        self.assertEqual(2, len(self.statistics.get_statistics()))

    def test_Against_I21(self):
        generator = word_generator(10, list())
        words = list()
        for i in range(5 * 10 ** 4):
            words.append(next(generator))

        start_time = default_timer()
        for word in words:
            self.statistics.add_word(word)
            self.assertLess(default_timer() - start_time, 1.0)
        for word in words:
            self.statistics.add_word(word)
            self.assertLess(default_timer() - start_time, 1.0)
        self.assertEqual(len(words), len(self.statistics.get_statistics()))


def word_generator(length, word):
    if length == 0:
        yield ''.join(word)
    else:
        for ordC in range(ord('a'), ord('z') + 1):
            c = chr(ordC)
            word.append(c)
            for result in word_generator(length - 1, word):
                yield result
            word.pop()
