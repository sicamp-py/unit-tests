"""Run this file to test all implementations"""

SUBMIT_URL = 'http://178.154.253.44:5000/post_data'
USE_UNITTEST = False

import sys
import os
sys.path.insert(0, os.path.join(os.path.dirname(__file__), ".."))

import inspect
import requests

from Challenge.word_statistics import WordStatistics as CorrectImplementation
from Challenge.word_statistics_tests import THE_AUTHOR

if USE_UNITTEST:
    from Challenge.DO_NOT_OPEN.unittest_stuff import test_implementation
    from Challenge.word_statistics_tests import WordStatistics_Tests
    tests_arg = WordStatistics_Tests
else:
    from Challenge.DO_NOT_OPEN.pytest_stuff import test_implementation
    tests_arg = ['-q', 'word_statistics_tests.py']


def get_incorrect_implementations():
    from Challenge.DO_NOT_OPEN import incorrect_implementations
    return [(name, item) for name, item in inspect.getmembers(incorrect_implementations) if name.startswith('WordStatistics_')]

def submit_result(who, tests):
    requests.post(SUBMIT_URL, json = {
        'who' : who,
        'tests' : tests
    })

def print_green(s=''):
    print('\x1b[1;32m' + s + '\x1b[0m')

def print_red(s=''):
    print('\x1b[1;31m' + s + '\x1b[0m')

def check_author(author):
    if author == '<ФАМИЛИЯ ИМЯ>':
        print_red('Заполните имя и фамилию в классе WordStatistics_Tests')
        exit(1)


check_author(THE_AUTHOR)

# Run correct implementation
was_successful, failed = test_implementation(CorrectImplementation, tests_arg)

if not was_successful:
    print_red('Корректная реализация не проходит следующие тесты: {0}'.format(', '.join(failed)))
    exit(0)

print_green('Корректная реализация проходит все тесты')

results = {}
killed_count = 0

for impl_name, impl in get_incorrect_implementations():
    was_successful, failed_tests = test_implementation(impl, tests_arg)
    results[impl_name] = (not was_successful, failed_tests)
    if was_successful:
        print_red('{0} проходит все тесты: напишите ещё чтобы завалить'.format(impl_name))
    else:
        killed_count += 1
        print_green('{0} падает на тестах: {1}'.format(impl_name, ', '.join(failed_tests)))

msg = 'Завалено плохих реализаций: {0}/{1}'.format(killed_count, len(results))
if killed_count == len(results):
    print_green(msg)
else:
    print_red(msg)

try:
    submit_result(THE_AUTHOR, results)
except:
    pass # вам не за чем об этом знать ^_^
