from collections import defaultdict


class WordStatistics:

    def __init__(self):
        self._stats = defaultdict(int)

    def add_word(self, word):
        if word is None:
            raise TypeError("word is None")
        if not isinstance(word, str):
            raise TypeError("word is not string")
        if len(word.strip()) == 0:
            return
        if len(word) > 10:
            word = word[:10]
        self._stats[word.lower()] += 1

    def get_statistics(self):
        """Returns words statistics in the form of list of tuple (int, str)
        ordered by int descending then by str ascending"""
        ordered = sorted(self._stats.items(), key=lambda pair: (-pair[1], pair[0]))
        return list(map(lambda pair: (pair[1], pair[0]), ordered))
