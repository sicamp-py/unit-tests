import pytest
from Challenge.word_statistics import WordStatistics

# Сначала впишите свою имя и фамилию
THE_AUTHOR = '<ФАМИЛИЯ ИМЯ>'

@pytest.fixture()
def statistics():
    return WordStatistics()

# Тестовые методы должны начинаться со слова 'test'

def test_statisticsEmptyAfterCreation(statistics):
    assert len(statistics.get_statistics()) == 0

def test_countOnce_WhenSameWord(statistics):
    statistics.add_word("aaaaaaaaaa")
    statistics.add_word("aaaaaaaaaa")
    assert len(statistics.get_statistics()) == 1

# Дальше - сами!
