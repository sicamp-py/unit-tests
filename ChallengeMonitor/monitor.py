from flask import Flask, request, render_template
app = Flask(__name__)

current_results = {}

@app.route("/post_data", methods=['POST'])
def post_data():
    data = request.get_json()
    current_results[data.get('who', 'Unknown')] = data.get('tests', None)
    return 'OK'

def get_killed_count(item):
    _, results = item
    return sum(1 for test_result, _ in results.values() if test_result)

def get_results_with_counts(results):
    def add_count(item):
        return (get_killed_count(item), item[0], item[1])
    return list(map(add_count, results))

@app.route('/')
def index():
    results = get_results_with_counts(current_results.items())
    results.sort(reverse=True)

    implementations = []
    if results:
        _, _, tests = results[0]
        implementations = [impl for impl in tests.keys()]
        implementations.sort()

    return render_template('index.html', results=results, implementations=implementations)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
