# Challenge:
* Прочитайте сначала данный README __полностью__.
* В файле *Challenge/word_statistics.py* есть класс `WordStatistics`. Разберитесь, что он делает.
* В каталоге _Challenge/DO_NOT_OPEN_ содержатся 20 классов, реализующих тот же интерфейс, что и `WordStatistics`,
 но в каждом из них есть ошибка. __Заглядывать в этот каталог запрещается в рамках данного задания__.
* Напишите в *Challenge/word_statistics_tests.py* набор тестов, на котором "упадёт" каждая неверная реализация. 
При этом верная реализация `WordStatistics` __должна их проходить__.
* Запускать тесты против верной реализации можно обычным образом.
* Чтобы прогнать тесты против всех реализаций, запустите файл *Challenge/run_this.py*.
* Не забудьте ввести свою фамилию и имя в файле с вашими тестами.

# Доп. материалы
 * [Документация py.test](https://docs.pytest.org/en/latest/)
 * [Доклад PyCon Russia 2016: Продвинутое использование py.test](http://pycon.ru/2016/program/content/svetlov/)
 * [Классная презенташка про py.test Fixtures](http://devork.be/talks/advanced-fixtures/advfix.html)