class Stack:
    def __init__(self, lst=list()):
        self._stack = list(lst)

    def push(self, item):
        self._stack.append(item)

    def pop(self):
        if len(self._stack) == 0:
            raise IndexError('Stack is empty')
        return self._stack.pop()

    def top(self):
        if len(self._stack) == 0:
            raise IndexError('Stack is empty')
        return self._stack[-1]

    def empty(self):
        return self.size() == 0

    def size(self):
        return len(self._stack)

    def to_list(self):
        return list(reversed(self._stack))