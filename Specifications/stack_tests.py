import pytest

from Specifications.stack import Stack


def test_defaultConstructor_createsEmptyStack():
    stack = Stack()
    assert stack.empty()


def test_popOnEmptyStack_fails():
    stack = Stack()
    with pytest.raises(IndexError):
        stack.pop()


def test_topOnEmptyStack_fails():
    stack = Stack()
    with pytest.raises(IndexError):
        stack.top()


def test_constructor_pushesItemsToEmptyStack():
    stack = Stack([1, 2, 3])
    assert stack.size() == 3
    assert stack.pop() == 3
    assert stack.pop() == 2
    assert stack.pop() == 1
    assert stack.size() == 0


def test_enumeration_returnsItemsInPopOrder():
    stack = Stack([1, 2, 3])
    assert stack.to_list() == [3, 2, 1]


def test_push_addItemToStackTop():
    stack = Stack([1, 2, 3])
    stack.push(42)
    assert stack.to_list() == [42, 3, 2, 1]


def test_pop_returnsLastPushedItem():
    stack = Stack([1, 2, 3])
    stack.push(42)
    assert stack.pop() == 42


def test_top_returnsLastPushedItem():
    stack = Stack([1, 2, 3])
    stack.push(42)
    assert stack.top() == 42


def test_size_returnsNumberOfItems():
    lst = [1, 2, 3]
    stack = Stack(lst)
    assert stack.size() == len(lst)


def test_pop_decreasesStackSize():
    stack = Stack([1, 2, 3])
    size = stack.size()
    stack.pop()
    assert stack.size() == size - 1


def test_top_shouldNotChangeStackSize():
    stack = Stack([1, 2, 3])
    size = stack.size()
    stack.top()
    assert stack.size() == size
